package de.spezialdefekt.fgalarm.util;

public class Commons {

    public final static String FG_URL =  "https://money.cnn.com/data/fear-and-greed";
    public final static int CONNECTION_TIMEOUT_IN_MS = 3000;

    // TODO put this stuff in @strings?
    public final static String PREF_LOWER_THRESHOLD = "PREF_LOWER_THRESHOLD";
    public final static String PREF_HIGHER_THRESHOLD = "PREF_HIGHER_THRESHOLD";
    public final static String PREF_CHOSEN_TIME = "PREF_CHOSEN_TIME";
    public final static String PREF_ALARM_MODE = "PREF_ALARM_MODE";
    public final static String PREF_LAST_FETCHED_FG = "PREF_LAST_FETCHED_FG";
    public final static String PREF_LAST_FETCHED_TIMESTAMP = "PREF_LAST_FETCHED_TIMESTAMP";

    private Boolean notifyUserOfConError = true;

    private int chosenHour;
    private int chosenMinute;

    private static Commons instance;
    private Commons() {}

    public static Commons getInstance () {
        if (Commons.instance == null) {
            Commons.instance = new Commons();
        }
        return Commons.instance;
    }

    public int getChosenHour() {
        return chosenHour;
    }

    public void setChosenHour(int chosenHour) {
        this.chosenHour = chosenHour;
    }

    public int getChosenMinute() {
        return chosenMinute;
    }

    public void setChosenMinute(int chosenMinute) {
        this.chosenMinute = chosenMinute;
    }

    public Boolean getNotifyUserOfConError() {
        return notifyUserOfConError;
    }

    public void setNotifyUserOfConError(Boolean notifyUserOfConError) {
        this.notifyUserOfConError = notifyUserOfConError;
    }
}

