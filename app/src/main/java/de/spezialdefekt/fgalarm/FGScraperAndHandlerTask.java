package de.spezialdefekt.fgalarm;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import de.spezialdefekt.fgalarm.util.Commons;

public class FGScraperAndHandlerTask extends AsyncTask<String, Integer, String> {

    private static final String TAG = "FGScraperAndHandlerTask";

    private Context context;

    public FGScraperAndHandlerTask(Context c){
        context = c;
    }

    @Override
    protected String doInBackground(String... strings) {
        Log.d(TAG, "Async Task called");

        String scrapedFG = scrapeFGSite();
        return scrapedFG;

    }

    @Override
    protected void onPostExecute(String scrapedFG) {
        Log.d(TAG, "Async Task done, doing post execute processing");

        super.onPostExecute(scrapedFG);

        if ("".equals(scrapedFG)) return;

        int fg = Integer.parseInt(scrapedFG);

        // save scraped fg to preferences, to be able to display the last fetched value
        savePreference(Commons.PREF_LAST_FETCHED_FG, String.valueOf(fg));
        savePreference(Commons.PREF_LAST_FETCHED_TIMESTAMP, new SimpleDateFormat("dd.MM.yyyy HH:mm:ss").format(Calendar.getInstance().getTime()));

        int lt = Integer.parseInt(loadStringPreference(Commons.PREF_LOWER_THRESHOLD));
        int ht = Integer.parseInt(loadStringPreference(Commons.PREF_HIGHER_THRESHOLD));


        // check if one of the thresholds was breached
        if (fg <= lt || fg >= ht)  {
            Log.d(TAG, "Threshold was breached, going to inform the user. lt: " + lt
                    + ", ht: " + ht
                    + ", fg: " + fg);
            notifyUserOfThresholdBreach(fg);
        } else  {
            Log.d(TAG, "No threshold was breached");
        }

        //TODO update textview in mainactvity

    }



    private String scrapeFGSite() {
        Log.d(TAG, "Start scraping");

        String scrapedFG = "";

        Document doc = null;

        try {
            doc = Jsoup.connect(Commons.FG_URL).timeout(Commons.CONNECTION_TIMEOUT_IN_MS).get();
        } catch (IOException e) {
            Log.e(TAG, "Error while trying to connect to f&g website, sleep for 5s and then try again");
            Log.getStackTraceString(e);

            try {
                Thread.sleep(5000);
            } catch (InterruptedException ie) {
                Log.e(TAG, "Thread was interrupted while sleeping");
                Log.getStackTraceString(ie);

                notifyUserOfConnectionProblem();
                return scrapedFG;
            }

            // try one more time
            try  {
                doc = Jsoup.connect(Commons.FG_URL).timeout(Commons.CONNECTION_TIMEOUT_IN_MS).get();
            } catch (IOException e2) {
                Log.e(TAG, "Error while second try to connect to f&g website");
                Log.getStackTraceString(e2);

                notifyUserOfConnectionProblem();
                return scrapedFG;
            }

        }

        Element elementContainingFGInfos = doc.getElementById("needleChart");

        // Log.d(TAG, "Found needleChart element: " + elementContainingFGInfos);

        Elements liElements = elementContainingFGInfos.getElementsByTag("li");

        // TODO null checks and proper e handling

        for (Element e : liElements) {

            Element withNow = e.getElementsContainingText("Now").first();

            if(withNow != null) {

                Log.d(TAG, "Fear & Greed including text: " + withNow.text());

                // filter digits
                scrapedFG = withNow.text().replaceAll("\\D+","");

                break;
            } else {
                //TODO: proper case handling
            }
        }

        Log.d(TAG, "Fear & Greed: " + scrapedFG);

        Log.d(TAG, "Scraping finished");
        return scrapedFG;
    }

    private void notifyUserOfThresholdBreach(int fg) {
        Log.d(TAG, "Start notify user of threshold breach");

        // TODO: make notifiction clickable, maybe redirect to cnn website? or maybe buttons for closing or for open website





        Intent notifyIntent = new Intent(context, MainActivity.class);
        // Set the Activity to start in a new, empty task
        notifyIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        // Create the PendingIntent
        PendingIntent notifyPendingIntent = PendingIntent.getActivity(context, 0, notifyIntent, PendingIntent.FLAG_UPDATE_CURRENT);


        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(context, "channelId")
                        .setSmallIcon(R.drawable.ic_launcher_foreground)
                        .setContentTitle("F&G Threshold was breached!")  // TODO put in @strings
                        .setContentText("F&G is currently at " + fg)  // TODO put in @strings
                        .setPriority(NotificationCompat.PRIORITY_HIGH)
                        // Set the intent that will fire when the user taps the notification
                        .setContentIntent(notifyPendingIntent)
                        .setAutoCancel(true);

        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(Context. NOTIFICATION_SERVICE ) ;

        notificationManager.notify(1, builder.build());

        Log.d(TAG, "End of notify user of threshold breach");
    }

    private void notifyUserOfConnectionProblem() {
        Log.d(TAG, "Start notify user of connection problem");

 //       if (!loadBooleanPreference(context.getString(R.string.preferences_notify_of_connection_error_key))) {

         if (!Commons.getInstance().getNotifyUserOfConError()) {
            Log.d(TAG, "User doesn't want to be notified of connection problem");
            return;
        }


        Intent notifyIntent = new Intent(context, MainActivity.class);
        // Set the Activity to start in a new, empty task
        notifyIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        // Create the PendingIntent
        PendingIntent notifyPendingIntent = PendingIntent.getActivity(context, 0, notifyIntent, PendingIntent.FLAG_UPDATE_CURRENT);


        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(context, "channelId")
                        .setSmallIcon(R.drawable.ic_launcher_foreground)
                        .setContentTitle("F&G Connection error")  // TODO put in @strings
                        .setContentText("F&G Alarm was unable to perform check")  // TODO put in @strings
                        .setStyle(new NotificationCompat.BigTextStyle().bigText("Check internet connection. Trying again in 24hs unless stopped or restarted."))
                        .setPriority(NotificationCompat.PRIORITY_HIGH)
                        .setContentIntent(notifyPendingIntent)
                        .setAutoCancel(true);


        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(Context. NOTIFICATION_SERVICE ) ;

        notificationManager.notify(2, builder.build());

        Log.d(TAG, "End of notify user of connection problem");
    }

    private void savePreference(String pref, String val)  {
        SharedPreferences preferences = context.getSharedPreferences(pref, 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(pref, val);
        editor.commit();
    }

    /*
    TODO: seriously, there must be a better way to do this without knowing the pref classification?!
     */

    private String loadStringPreference(String pref)  {
        SharedPreferences preferences = context.getSharedPreferences(pref, 0);
        return preferences.getString(pref, "");
    }

    private Boolean loadBooleanPreference(String pref)  {

        // TODO: where are all the prefs from the other activities?!?
        // edit: would work if you use default pref in all the other activities, but then listener throws exception

        SharedPreferences preferences = context.getSharedPreferences(pref, 0);

        Log.d(TAG, pref + " exists?: " + preferences.contains(pref));

        return preferences.getBoolean(pref, true);
    }

}