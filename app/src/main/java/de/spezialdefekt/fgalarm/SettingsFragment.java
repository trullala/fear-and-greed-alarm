package de.spezialdefekt.fgalarm;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;

import androidx.preference.PreferenceFragmentCompat;

import java.util.Map;

import de.spezialdefekt.fgalarm.util.Commons;


public class SettingsFragment extends PreferenceFragmentCompat implements SharedPreferences.OnSharedPreferenceChangeListener {

    private static final String TAG = "SettingsFragment";

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {

        Log.d(TAG, "onCreatePreferences, list of already existing ones:");

        // TODO: where are all the prefs from the other activities?!?
        // edit: now it works ... had to use default pref in all the other activities


        Map<String, ?> map = getPreferenceManager().getSharedPreferences().getAll();

        for (Map.Entry<String, ?> entry : map.entrySet()) {
            Log.d(TAG, entry.getKey() + ":" + entry.getValue());
        }

        // load preference elements from xml
        addPreferencesFromResource(R.xml.root_preferences);

        getPreferenceManager().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);


//        SharedPreferences preferences = findPreference(Commons.PREF_KEY_SWITCH_NOTIFY_CONNERR);
 //       preference.registerOnSharedPreferenceChangeListener(this);

    }

 /*  @Override
    public boolean onPreferenceChange(Preference preference, Object newValue) {
        String preferenceKey = preference.getKey();
        boolean preferenceValue = (boolean) newValue;

        if (preferenceKey.equals(Commons.PREF_KEY_SWITCH_NOTIFY_CONNERR)) {
            preference.setSummary("Der neue Wert ist: " + preferenceValue);
        }
        return true;
    } */

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        Log.d(TAG, "Setting changed: " +  key);

        Map<String, ?> map = sharedPreferences.getAll();

        for (Map.Entry<String, ?> entry : map.entrySet()) {
            Log.d(TAG, entry.getKey() + ":" + entry.getValue());
        }


        if (key.equals("preferences_notify_of_connection_error_key")) {
               // getString(R.string.preferences_notify_of_connection_error_key))) {
            Log.d(TAG, "Setting changed: " +  key + " to: " + sharedPreferences.getBoolean(key, true));

            // TODO it's not a good idea to do this via commons I think. I don't know how to do it with sharedpreferences, as I can't seem to read them from FGHandler, somethings wrong with the context then
            Commons.getInstance().setNotifyUserOfConError(sharedPreferences.getBoolean(key, true));

        }
    }
}
