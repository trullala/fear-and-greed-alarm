package de.spezialdefekt.fgalarm;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class BroadcastReceiver extends android.content.BroadcastReceiver {

    private static final String TAG = "BroadcastReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(TAG, "Alarm was fired, call async scraper");

        scrapeFG(context);
    }

    private void scrapeFG(Context context) {

        FGScraperAndHandlerTask scraper =  new FGScraperAndHandlerTask(context);

        scraper.execute();
    }

}