package de.spezialdefekt.fgalarm;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

import android.app.AlarmManager;
import android.app.Dialog;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import de.spezialdefekt.fgalarm.util.Commons;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    // Editables
    EditText etnLowerThreshold, etnHigherThreshold;
    // Displays of settings
    TextView tvLowerThreshold, tvHigherThreshold, tvTime, tvCurrentAlarmState;
    // Display of fetched value
    TextView tvLastFetchedFG;
    TextView tvLastFetchedTimestamp;

    TextView tvAppVersion;

    int chosenHour, chosenMinute;

    final int FG_MIN = 0;
    final int FG_MAX = 100;

    final String ON = "ON";
    final String OFF = "OFF";


    AlarmManager alarmManager;
    PendingIntent pendingIntent;

    Commons commons;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "MainActivity created");

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        etnLowerThreshold = findViewById(R.id.etnLowerThreshold);
        etnHigherThreshold =  findViewById(R.id.etnHigherThreshold);

        tvLowerThreshold = findViewById(R.id.tvLowerThreshold);
        tvHigherThreshold = findViewById(R.id.tvHigherThreshold);
        tvTime = findViewById(R.id.tvSetTime);
        tvCurrentAlarmState = findViewById(R.id.tvCurrentAlarmState);

        tvLastFetchedFG = findViewById(R.id.tvLastFetchedFG);
        tvLastFetchedTimestamp = findViewById(R.id.tvLastFetchedTimestamp);

        tvAppVersion = findViewById(R.id.tvAppVersion);
        tvAppVersion.setText("App Version: " + getResources().getString(R.string.app_version));
    }

    @Override
    protected void onStart() {
        Log.d(TAG, "MainActivity started");

        super.onStart();

        commons = Commons.getInstance();

        Intent intent = new Intent(this, BroadcastReceiver.class);
        intent.setAction("BackgroundProcess");

        alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        pendingIntent = PendingIntent.getBroadcast(this, 0, intent, 0);

        createNotificationChannel();

    }

    @Override
    protected void onResume() {
        Log.d(TAG, "MainActivity resumed");

        super.onResume();

        // load set values from last time, if any
        etnLowerThreshold.setText(loadPreference(Commons.PREF_LOWER_THRESHOLD));
        etnHigherThreshold.setText(loadPreference(Commons.PREF_HIGHER_THRESHOLD));
        tvLowerThreshold.setText(loadPreference(Commons.PREF_LOWER_THRESHOLD));
        tvHigherThreshold.setText(loadPreference(Commons.PREF_HIGHER_THRESHOLD));
        tvTime.setText(loadPreference(Commons.PREF_CHOSEN_TIME));
        tvCurrentAlarmState.setText(loadPreference(Commons.PREF_ALARM_MODE));
        tvLastFetchedFG.setText(loadPreference(Commons.PREF_LAST_FETCHED_FG));
        tvLastFetchedTimestamp.setText(loadPreference(Commons.PREF_LAST_FETCHED_TIMESTAMP));

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            // launch settings activity
            startActivity(new Intent(MainActivity.this, SettingsActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStop() {
        Log.d(TAG, "MainActivity stopped");

        super.onStop();
    }

    @Override
    protected void onDestroy() {
        Log.d(TAG, "MainActivity destroyed");

        super.onDestroy();
    }



    public void startAlarmButtonClick(View view) {
        Log.d(TAG, "Start Button clicked");

        Editable ltText = etnLowerThreshold.getText();
        Editable htText = etnHigherThreshold.getText();

        int lt;
        int ht;

        // check if ht is not accidently lower than lt
        if(ltText != null && !ltText.toString().equals("")
                && htText != null && !htText.toString().equals("")
                && Integer.parseInt(htText.toString()) < Integer.parseInt(ltText.toString())) {

            Toast.makeText(getApplicationContext(),
                    "Would make more sense if the lower threshold is smaller than the higher one, eh?", // TODO put in @strings
                    Toast.LENGTH_LONG).show();
            return;
        }

        // if no value is given, we'll use min and max per default
        if (ltText != null && !ltText.toString().equals("")) {
            lt = Integer.parseInt(ltText.toString());
        } else lt = FG_MIN;
        if (htText != null && !htText.toString().equals("") && Integer.parseInt(htText.toString()) <= 100) {
            ht = Integer.parseInt(htText.toString());
        } else ht = FG_MAX;



        chosenHour = commons.getChosenHour();
        chosenMinute = commons.getChosenMinute();

        // TODO: we should probably use LocalDateTime, but that would require min API 26
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.set(Calendar.HOUR_OF_DAY, chosenHour);
        calendar.set(Calendar.MINUTE, chosenMinute);

        String chosenTime = new SimpleDateFormat("HH:mm").format(calendar.getTime());

        // cancel previous alarm if set
        if (alarmManager!= null) {
            Log.d(TAG, "Cancel previous alarm");
            alarmManager.cancel(pendingIntent);
        }

        //start the (new) alarm
        alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),
                AlarmManager.INTERVAL_DAY, pendingIntent);

        Log.d(TAG, "Alarm started, will fire every day at approximately " + chosenTime + " with thresholds: lt: " + lt + ", ht: " + ht);


        // set textfields to show what user chose
        tvLowerThreshold.setText(String.valueOf(lt));
        tvHigherThreshold.setText(String.valueOf(ht));
        tvTime.setText(chosenTime);
        tvCurrentAlarmState.setText(ON);

        // safe variables to preferneces so they reappear when you close and reopen the app
        savePreference(Commons.PREF_LOWER_THRESHOLD, String.valueOf(lt));
        savePreference(Commons.PREF_HIGHER_THRESHOLD, String.valueOf(ht));
        savePreference(Commons.PREF_CHOSEN_TIME, chosenTime);
        savePreference(Commons.PREF_ALARM_MODE, ON);


        Toast.makeText(getApplicationContext(),
                "Alarm started. Checks index every day at approximately " + chosenTime + " until manually stopped!", // TODO put in @strings
                Toast.LENGTH_LONG).show();


        //    finish();

    }

    public void stopAlarmButtonClick(View view) {
        Log.d(TAG, "Stop Button clicked");

        // If the alarm has been set, cancel it.
        if (alarmManager!= null) {
            alarmManager.cancel(pendingIntent);
        }

        Log.d(TAG,"Alarm will no longer fire!");

        // clear displayed values
        tvLowerThreshold.setText("");
        tvHigherThreshold.setText("");
        tvTime.setText("");
        tvCurrentAlarmState.setText(OFF);

        // update / empty saved preferences
        savePreference(Commons.PREF_LOWER_THRESHOLD, "");
        savePreference(Commons.PREF_HIGHER_THRESHOLD, "");
        savePreference(Commons.PREF_CHOSEN_TIME, "");
        savePreference(Commons.PREF_ALARM_MODE, OFF);

    }

    public void showTimePickerDialog(View v) {
        Log.d(TAG, "Time Picking Button clicked");

        DialogFragment newFragment = new TimePickerFragment();
        newFragment.show(getSupportFragmentManager(), "timePicker");
    }

    private void createNotificationChannel() {
        // Starting in Android 8.0 (API level 26), all notifications must be assigned to a channel.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) { // 0 is 26 ...
            CharSequence name = "FG Notifications Channel";
            String description = "All Notifications from the FG Alarm app";
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel channel = new NotificationChannel("channelId", name, importance);
            channel.setDescription(description);

            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    private void savePreference(String pref, String val)  {
    SharedPreferences preferences = getSharedPreferences(pref, MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(pref, val);
        editor.commit();
    }

    private String loadPreference(String pref)  {
        SharedPreferences preferences = getSharedPreferences(pref, MODE_PRIVATE);
        return preferences.getString(pref, "");
    }

    public static class TimePickerFragment extends DialogFragment
            implements TimePickerDialog.OnTimeSetListener {

        private static final String TAG = "TimePickerFragment";

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current time as the default values for the picker
            final Calendar c = Calendar.getInstance();
            int hour = c.get(Calendar.HOUR_OF_DAY);
            int minute = c.get(Calendar.MINUTE);

            return new TimePickerDialog(getActivity(), this, hour, minute,
                    true);
                   // DateFormat.is24HourFormat(getActivity()));
        }

        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            Log.d(TAG, "Time set: " + hourOfDay + ":" + minute);

            Commons.getInstance().setChosenHour(hourOfDay);
            Commons.getInstance().setChosenMinute(minute);

        }
    }


}